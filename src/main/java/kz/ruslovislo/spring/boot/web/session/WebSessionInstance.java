package kz.ruslovislo.spring.boot.web.session;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Ruslan Temirbulatov on 11/13/19
 * @project spring-boot-web
 * <p>
 *     The spring boot web session bean to hold any session scoped parameters, i.e. username.
 * </p>
 */
public class WebSessionInstance {


    public static WebSessionInstance get() {

        ServletRequestAttributes attrs = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes());
        if(attrs==null)
            return null;

        HttpServletRequest request = attrs.getRequest();


        HttpSession session = request.getSession(true);// true will create if necessary

        return get(session);

    }
    public static WebSessionInstance get(HttpSession session) {
        WebSessionInstance instance = (WebSessionInstance)session.getAttribute(WebSessionInstance.class.getName());

        if (instance == null) {
            instance = new WebSessionInstance();
            session.setAttribute(WebSessionInstance.class.getName(), instance);
        }
        return instance;


    }

    public Map<String,Object> params = new HashMap<String, Object>();


}
